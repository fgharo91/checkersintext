public class CheckerPiece {
	// A checker piece has
	// - A color, we might not need this.
	// - Can move in the forward direction only.
	// - Can be in the game or has been jumped.
	// - A checker piece can move or jump another checker piece.
	private char color;
	private int pieceNum;
	protected int row;
	protected int column;
	private boolean king = false;
	private boolean bottom; // Will work as a flag to see if the checker piece started at the bottom which means it can move in the upper direction.
	
		
	public CheckerPiece(boolean startedAtBottom, int pieceNum) {
		// Can perhaps always keep track of this by having 4 methods that automatically set this when a checker piece is made.		
		bottom = startedAtBottom;
		this.pieceNum = pieceNum;
	}
	
	// For setting the color of this checkerPiece.
	public void setColor(char c) {
		color = c;
	}
	
	// For setting this CheckerPieces position
	public void setPos(int r, int c) {
		row = r; column = c;
	}
	
	// For getting the row and column	
	public int[] getCoor() {
		return new int[]{row, column};
	}
	
	
	// Initialize neighbors 

	// For setting the state jumped of this checkerPiece.
	public void jumpMe() {
		GameField.clearSpace(row, column);		
		// NOTE make sure this does not cause any errors!!!
		row = -1;
		column = -1;
	}
	
	// For setting the state king of a checkerPiece.
	public void kingMe() {
		System.out.println(toString() + " Has been crowned!");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(getColor() == 'r'){
			setColor('R');
			GameField.crownPiece(row,column,'r','R');
		}else{
			setColor('B');
			GameField.crownPiece(row,column,'b','B');
		}
		king = true;
	}
	
	// For knowing if this piece has been jumped.
	public boolean isJumped() {
		return (row == -1 && column == -1);
	}
	
	// For knowing if this piece is a king.
	public boolean isKing() {
		return king;
	}
	
	// For accessing the color
	public char getColor() {
		return color;
	}
	
	// Will calculate if the checker piece can move in a certain direction.
	public boolean hasUpperLeft() {
		return !(row == 0 || column == 0) 
				&& GameField.getFieldElement(this.getUpperLeft()[0], this.getUpperLeft()[1]).equals(GameField.whiteSpace);			  
	}
	public boolean hasUpperRight(){
		return !(row == 0 || column == 7)
				&& GameField.getFieldElement(this.getUpperRight()[0], this.getUpperRight()[1]).equals(GameField.whiteSpace);
	}
	
	public boolean hasLowerLeft(){
		return !(row == 7 || column == 0)
				&& GameField.getFieldElement(this.getLowerLeft()[0], this.getLowerLeft()[1]).equals(GameField.whiteSpace);
	}
	public boolean hasLowerRight() {
		return !(row == 7 || column == 7)
				&& GameField.getFieldElement(this.getLowerRight()[0], this.getLowerRight()[1]).equals(GameField.whiteSpace);
	}
	
	public int[] getUpperLeft() { 
		return new int[]{this.row-1, this.column -1};
	}
	
	public int[] getUpperRight() { 
		return new int[]{this.row-1, this.column+1};
	}
	
	public int[] getLowerLeft() {
		return new int[]{this.row+1, this.column-1};
	}
	
	public int[] getLowerRight() {
		return new int[]{this.row+1, this.column+1};
	}
	
	/**
	 * TODO Does not check if 
	 * @return
	 */
	public boolean canJump(CheckerPiece opLowerLeft,CheckerPiece opLowerRight,CheckerPiece opUpperLeft,CheckerPiece opUpperRight) {
		if(isJumped()) return false;
		// a king will look in all directions to see if they can jump.
		if(king){
			// lower left.
			if(opLowerLeft != null && !(opLowerLeft.getCoor()[0] == 7 || opLowerLeft.getCoor()[1] == 0)){
				if(GameField.getFieldElement(opLowerLeft.getLowerLeft()[0], opLowerLeft.getLowerLeft()[1]).equals(GameField.whiteSpace))
					return true;
			}
			// lower right.
			if(opLowerRight != null && !(opLowerRight.getCoor()[0] == 7 || opLowerRight.getCoor()[1] == 7)){
				if(GameField.getFieldElement(opLowerRight.getLowerRight()[0], opLowerRight.getLowerRight()[1]).equals(GameField.whiteSpace))
					return true;
			}
			// upper left.
			if(opUpperLeft != null && !(opUpperLeft.getCoor()[0] == 0 || opUpperLeft.getCoor()[1] == 0)){
				if(GameField.getFieldElement(opUpperLeft.getUpperLeft()[0], opUpperLeft.getUpperLeft()[1]).equals(GameField.whiteSpace))
					return true;
			}
			// upper right.
			if(opUpperRight != null && !(opUpperRight.getCoor()[0] == 0 || opUpperRight.getCoor()[1] == 7)){
				if(GameField.getFieldElement(opUpperRight.getUpperRight()[0], opUpperRight.getUpperRight()[1]).equals(GameField.whiteSpace))
					return true;
			}

		}
			
			// a piece that started at bottom will look for surrounding spots above it.
		else{ if(bottom)
				{			
					// upper left.
					if(opUpperLeft != null && !(opUpperLeft.getCoor()[0] == 0 || opUpperLeft.getCoor()[1] == 0)){
						if(GameField.getFieldElement(opUpperLeft.getUpperLeft()[0], opUpperLeft.getUpperLeft()[1]).equals(GameField.whiteSpace))
							return true;
					}
					// upper right.
					if(opUpperRight != null && !(opUpperRight.getCoor()[0] == 0 || opUpperRight.getCoor()[1] == 7)){
						if(GameField.getFieldElement(opUpperRight.getUpperRight()[0], opUpperRight.getUpperRight()[1]).equals(GameField.whiteSpace))
							return true;
					}
				}
			// a piece that started at top will look for surrounding spots below it.
				else{	
					// lower left.
					if(opLowerLeft != null && !(opLowerLeft.getCoor()[0] == 7 || opLowerLeft.getCoor()[1] == 0)){
						if(GameField.getFieldElement(opLowerLeft.getLowerLeft()[0], opLowerLeft.getLowerLeft()[1]).equals(GameField.whiteSpace))
							return true;
					}
					// lower right.
					if(opLowerRight != null && !(opLowerRight.getCoor()[0] == 7 || opLowerRight.getCoor()[1] == 7)){
						if(GameField.getFieldElement(opLowerRight.getLowerRight()[0], opLowerRight.getLowerRight()[1]).equals(GameField.whiteSpace))
							return true;
					}
				}
		}
		
		return false;

	}
	
	/**
	 * For peeking at surrounding free spots of a checker piece.
	 * @return true if there is a free spot that the checker piece can move into. false otherwise.
	 */
	public boolean canMove() {
		
		// Should we only care about moving forward? This is relative to whether you on the lower side of the board vs the upper side of the board 
		// and changes when a checker piece becomes a king. Perhaps we can set an two variables. If a checker piece is on the lower side of the board they
		// can only move towards the top of the board. If a checker piece is a king they can move in all directions.
		if(isJumped()){ return false;}
		
		if(king)
			return hasUpperLeft() || hasUpperRight() || hasLowerLeft() || hasLowerRight();
		else if(bottom)
				return hasUpperLeft() || hasUpperRight();
		else	//{System.out.println(toString()+"started at top. Can move?" + (hasLowerLeft() || hasLowerRight()) + " LL? " +hasLowerLeft()+ " LR? "+hasLowerRight()); 
				return hasLowerLeft() || hasLowerRight();
	}
	
	public boolean startedAtBottom() {
		return bottom;
	}
	
	public void moveToUpperLeft() {
		// if so move the piece on the GameField checker board from its original coordinates into the new upper right coordinates.
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], this.getUpperLeft()[0], this.getUpperLeft()[1]);
		// Don't forget to update the coordinates of the checker piece.
		this.setPos(this.getUpperLeft()[0],  this.getUpperLeft()[1]);
		
		// check if piece should be kinged.
		if(startedAtBottom() && !isKing() && this.getCoor()[0] == 0){
			kingMe();		
		}
	}
	
	public void moveToUpperRight() {
		// if so move the piece on the GameField checker board from its original coordinates into the new upper right coordinates.
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], this.getUpperRight()[0], this.getUpperRight()[1]);
		// Don't forget to update the coordinates of the checker piece.
		this.setPos(this.getUpperRight()[0],  this.getUpperRight()[1]);
		
		// check if piece should be kinged.
		if(startedAtBottom() && !isKing() && this.getCoor()[0] == 0){
			kingMe();		
		}
	}
	
	public void moveToLowerLeft() {
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], this.getLowerLeft()[0], this.getLowerLeft()[1]);
		this.setPos( this.getLowerLeft()[0], this.getLowerLeft()[1]);
		
		// check if piece should be kinged.
		if(!startedAtBottom() && !isKing() && this.getCoor()[0] == 7){
			kingMe();		
		}
	}
	
	public void moveToLowerRight() {
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], this.getLowerRight()[0], this.getLowerRight()[1]);
		this.setPos( this.getLowerRight()[0], this.getLowerRight()[1]);
		
		// check if piece should be kinged.
		if(!startedAtBottom() && !isKing() && this.getCoor()[0] == 7){
			kingMe();		
		}
	}
	
	public void jumpLowerLeft(CheckerPiece pieceToJump) {
		// Move jumping piece to space behind pieceToJump
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], pieceToJump.getLowerLeft()[0], pieceToJump.getLowerLeft()[1]);
		this.setPos(pieceToJump.getLowerLeft()[0], pieceToJump.getLowerLeft()[1]);
		
		// Clear the jumping piece off the field.
		pieceToJump.jumpMe();
		
		// check if piece should be kinged.
		if(!startedAtBottom() && !isKing() && this.getCoor()[0] == 7){
			kingMe();
		}
	}
	
	public void jumpLowerRight(CheckerPiece pieceToJump) {
		// Move jumping piece to space behind pieceToJump
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], pieceToJump.getLowerRight()[0], pieceToJump.getLowerRight()[1]);
		this.setPos(pieceToJump.getLowerRight()[0], pieceToJump.getLowerRight()[1]);
		
		// Clear the jumping piece off the field.
		pieceToJump.jumpMe();
		
		// check if piece should be kinged.
		if(!startedAtBottom() && !isKing() && this.getCoor()[0] == 7){
			kingMe();	
		}
		
	}
	
	public void jumpUpperLeft(CheckerPiece pieceToJump) {
		// Move jumping piece to space behind pieceToJump
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], pieceToJump.getUpperLeft()[0], pieceToJump.getUpperLeft()[1]);
		this.setPos(pieceToJump.getUpperLeft()[0], pieceToJump.getUpperLeft()[1]);
		
		// Clear the jumping piece off the field.
		pieceToJump.jumpMe();
		
		// check if piece should be kinged.
		if(startedAtBottom() && !isKing() && this.getCoor()[0] == 0){

			kingMe();
		}
	}
	
	public void jumpUpperRight(CheckerPiece pieceToJump) {
		// Move jumping piece to space behind pieceToJump
		GameField.movePiece(this.getCoor()[0], this.getCoor()[1], pieceToJump.getUpperRight()[0], pieceToJump.getUpperRight()[1]);
		this.setPos(pieceToJump.getUpperRight()[0], pieceToJump.getUpperRight()[1]);
		
		// Clear the jumping piece off the field.
		pieceToJump.jumpMe();
		
		// check if piece should be kinged.
		if(startedAtBottom() && !isKing() && this.getCoor()[0] == 0){

			kingMe();
		}
	}
	
	public String toString() {
		return this.color + String.valueOf(this.pieceNum);
	}
}
