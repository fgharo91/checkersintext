import java.util.Scanner;


// Started on Monday, June 3: 4 hours
// Worked on it Tuesday, June 4: 4 hours
// Wednesday, June 5: 4 hours
public class CheckersGame {
	// Every checker game has a red player, black player and a board.
	boolean redsTurn = true;
	Player redPlayer;
	Player blackPlayer;
	
	// Every checker game has options for playing.
	String welcomePrompt ="Welcome to CHECKERS! Would you like to play? yes(y) or no(n) ? Enter y or n: ";
	String choicePrompt = "What will you do? (m) move checker piece (q) quit. Enter m/q: ";
	String choicePrompt2 = "Did you want to play again? (y/n): ";
	String startMess = "Red Player starts the first move! Lets begin!";
	String wonMess1 = "Red Player has won!";
	String wonMess2 = "Black Player has won!";

	
	public void startGame() {
		Scanner read = new Scanner(System.in);	
		char choice = 'z';

		System.out.print(welcomePrompt);
		choice = read.next().charAt(0);

		// Now we want the human player to keep moving pieces until they either lose or win.
		// The player should have options a) if they want to move. b) if they want to quit the present game.	
		CheckerPiece[] moveablePieces = null;

		while(choice== 'y' || choice == 'Y')
		{	
			// Make some players for the game, R will be red and B will be black.
			redPlayer= new Player('r', false);
			blackPlayer= new Player('b', true);

			// Make sure both players can observe the others pieces.
			redPlayer.lookAtOppPieces(blackPlayer.getPieces());
			blackPlayer.lookAtOppPieces(redPlayer.getPieces());

			// Put Player pieces on the checker board.
			GameField.initializeField();
			GameField.setField( blackPlayer.getPieces(),redPlayer.getPieces());

			// Print the board.
			GameField.printBoard();
			System.out.println(startMess);

			do{
				
				if(winner()) break;
				
				printPlayerOptions();
				choice = read.next().charAt(0);
				
				switch(choice)
				{
				// if a player has jumps, they must jump before moving.
				// else a player can just move.
				case 'm':
					int i = 0;
					int piece = 0;
					String whichPieceToMove= "",whichPiecesToJumpWith= "";
					
					// Red Players Turn
					if(redsTurn){
						// Red Player jumps.
						if(redPlayer.canJump()){
							System.out.println("Which piece will you jump with? ");
							moveablePieces = redPlayer.getJumpingCheckerPieces();
							
							
							while(i< moveablePieces.length){
								if(moveablePieces[i] != null)
									whichPiecesToJumpWith+="Enter " + i + " for " + moveablePieces[i].toString() + " |";
								i++;						
							}

							do{
								System.out.print(whichPiecesToJumpWith + " : ");
								piece = read.nextInt();

								if((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null){
									System.out.println("Invalid Option!");
								}
							}while((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null);
							
							redPlayer.jump(moveablePieces[piece]);
							
							
							// Red Player moves.
						}else{
							System.out.println("Which piece will you move? ");
							moveablePieces = redPlayer.getMovableCheckerPieces();
							while(i< moveablePieces.length){
								if(moveablePieces[i] != null)
									whichPieceToMove+="Enter " + i + " for " + moveablePieces[i].getColor() + i + " |";
								i++;						
							}
							
							// Get Player's choice.
							do{
								System.out.print(whichPieceToMove + " : ");
								piece = read.nextInt();

								if((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null){
									System.out.println("Invalid Option!");
								}
							}while((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null);
							
							redPlayer.move(moveablePieces[piece]);
						}
					// Black Players Turn.
					}else{
						// Black Player jumps.
						if(blackPlayer.canJump()){
							System.out.println("Which piece will you jump with? ");
							moveablePieces = blackPlayer.getJumpingCheckerPieces();
							
							while(i< moveablePieces.length){
								if(moveablePieces[i] != null)
									whichPiecesToJumpWith+="Enter " + i + " for " + moveablePieces[i].toString() + " |";
								i++;						
							}

							do{
								System.out.print(whichPiecesToJumpWith + " : ");
								piece = read.nextInt();

								if((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null){
									System.out.println("Invalid Option!");
								}
							}while((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null);
							
							blackPlayer.jump(moveablePieces[piece]);
							// Black Player moves.
						}else{							
							System.out.println("Which piece will you move? ");
							moveablePieces = blackPlayer.getMovableCheckerPieces();
							
							// Show player the choices.
							while(i< moveablePieces.length){
								if(moveablePieces[i] != null)
									whichPieceToMove+="Enter " + i + " for " + moveablePieces[i].getColor() + i + " |";
								i++;						
							}
							
							// Get Player's choice.
							do{
								System.out.print(whichPieceToMove + " : ");
								piece = read.nextInt();

								if((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null){
									System.out.println("Invalid Option!");
								}
							}while((piece < 0 || piece >= moveablePieces.length) || moveablePieces[piece] == null);
							
							blackPlayer.move(moveablePieces[piece]);
							
						}
					}// We will call player.move() method here.					
					GameField.printBoard();
					redsTurn=!redsTurn;
					
					break;
				case 'q':
					if(redsTurn)
						System.out.println(wonMess2);
					else
						System.out.println(wonMess1);

					break;
				default: System.out.println("Invalid option!\n");

				}	
			}while(choice !='q');
			System.out.print(choicePrompt2);
			choice = read.next().charAt(0);	
		}

		read.close();
		System.out.println("Farewell!");

	}
	
	/**
	 * Checks for a winner of the Checkers Game.
	 * @return true if there was a winner, false otherwise.
	 */
	public boolean winner() {
		if(redsTurn && redPlayer.hasLost()){
			System.out.println(wonMess2); return true;
		}else{
			if(!redsTurn && blackPlayer.hasLost()){
				System.out.println(wonMess1); return true;
			}else return false;
		}
		
	}
	
	public void printPlayerOptions() {
		if(redsTurn)
			System.out.println(redPlayer + "'s turn");
		else
			System.out.println(blackPlayer + "'s turn");
		System.out.print(choicePrompt);
	}
	 
	/**
	 * In a n-dimensional array the most inner brace refers to the most inner dimension. 
	 * For example, int array[] = {a,b,c,...,z} has only one inner dimension.
	 * int array[][] = {a,b,c,d,...,z} where a = {a2,b2,c2,d2,...,z2} and b, c, d can also have other arrays and
	 * a2,b2,d2,...,z2 are individual elements.
	 *  
	 */
	public static void main(String[] args) {
		(new CheckersGame()).startGame();		
	}
}

