public class GameField  {
	// -Has an array of checker positions
	// -Each position can either be taken or free to move on.
	// -Certain positions 
	private static String field[][];
	public static String whiteSpace = "        ";
	public static String blackSpace = "||||||||";
	
	// Makes the checker board field.
	public static void initializeField() {
		field = new String[8][8];
		
		// for when i is even.
		for(int i=0;i<8;i=i+2)
		for(int j=0;j<8;j++)
			if(j%2 == 0){
				field[i][j] = whiteSpace;
			}else{
				field[i][j] = blackSpace;
			}
		
		// for when i is odd.
		for(int i=1;i<8;i=i+2)
		for(int j=0;j<8;j++)
			if(j%2 == 0){
				field[i][j] = blackSpace;
			}else{
				field[i][j] = whiteSpace;
			}
	}
	
	// Initializes the players checker pieces on the board.
	public static void setField(CheckerPiece player1Pieces[], CheckerPiece player2Pieces[])
	{
		// if i < 10 print out: "   " + color + i + "   " else print out: "  " + color + i + "   "
		// For player 1
		int j = 0;
		// initialize row 7
		for(int i = 1; i <= 7;i = i+2){
			
			field[7][i]= (j < 10 ?
					"   " + player1Pieces[j].getColor()+ j + "   " :
					"  " + player1Pieces[j].getColor() + j + "   ");
			player1Pieces[j].setPos(7, i);
			j++;
		}
		
		// initialize row 6
		for(int i = 0; i <= 7;i = i+2){
			field[6][i] =(j < 10 ?
					"   " + player1Pieces[j].getColor()+ j + "   " :
					"  " + player1Pieces[j].getColor() + j + "   ");
			player1Pieces[j].setPos(6, i);
			j++;
		}
		
		// initialize row 5
		for(int i = 1; i <= 7;i = i+2){
			field[5][i] =(j < 10 ?
					"   " + player1Pieces[j].getColor()+ j + "   " :
					"  " + player1Pieces[j].getColor() + j + "   ");
			player1Pieces[j].setPos(5, i);
			j++;
		}
		
		// For player 2
		int k = 0;
		// initialize row 0
		for(int i = 0; i <= 7;i = i+2){
			field[0][i]=(k < 10 ?
						"   " + player2Pieces[k].getColor()+ k + "   " :
						"  " + player2Pieces[k].getColor() + k + "   ");
			player2Pieces[k].setPos(0, i);
			k++;
		}
		
		// initialize rows 1
		for(int i = 1; i <= 7;i = i+2){
			field[1][i]=(k < 10 ?
					"   " + player2Pieces[k].getColor()+ k + "   " :
					"  " + player2Pieces[k].getColor() + k + "   ");
			player2Pieces[k].setPos(1, i);
			k++;
		}
		
		// initialize row 2
		for(int i = 0; i <= 7;i = i+2){
			field[2][i]=(k < 10 ?
					"   " + player2Pieces[k].getColor()+ k + "   " :
					"  " + player2Pieces[k].getColor() + k + "   ");
			player2Pieces[k].setPos(2, i);
			k++;
		}
		
	}
	
	// Prints the checker board to standard output.
	public static void printBoard(){
		 for(int i=0;i<20;i++)
			 System.out.println();
		 System.out.println("\n=========================== CHECKERS ===========================" + "\n" +
							"================================================================");
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
		 for(int i = 0;i<8;i++)
		 System.out.print(field[0][i]); // Prints out first row of the checker board.
		 System.out.println();
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
	//	 System.out.println();
	//	 System.out.println("\n================================================================"); 
		 
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace); 
		 for(int i = 0;i<8;i++)
		 System.out.print(field[1][i]); // Prints out second row of the checker board.
		 System.out.println();
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace);
	//	 System.out.println();
	//	 System.out.println("\n================================================================"); 
		 
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
		 for(int i = 0;i<8;i++)
		 System.out.print(field[2][i]); // Prints out third row of the checker board.
		 System.out.println();
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);		 
	//	 System.out.println();
	//	 System.out.println("\n================================================================"); 
		
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace); 
		 for(int i = 0;i<8;i++)
		 System.out.print(field[3][i]); // Prints out fourth row of the checker board.
		 System.out.println();
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace);
	//	 System.out.println();
	//	 System.out.println("\n================================================================"); 
		
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
		 for(int i = 0;i<8;i++)
		 System.out.print(field[4][i]); // Prints out fifth row of the checker board.
		 System.out.println();
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
	//	 System.out.println();
	//	 System.out.println("\n================================================================"); 
		 
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace);
		 for(int i = 0;i<8;i++)
		 System.out.print(field[5][i]); // Prints out sixth row of the checker board.
		 System.out.println();
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace);
	//	 System.out.println();
	//	 System.out.println("\n================================================================"); 
		 
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
		 for(int i = 0;i<8;i++)
		 System.out.print(field[6][i]); // Prints out seventh row of the checker board.
		 System.out.println();
		 System.out.println(whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace);
	//	 System.out.println();
		// System.out.println("\n================================================================"); 
		 
		
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace);
		 for(int i = 0;i<8;i++)
		 System.out.print(field[7][i]); // Prints out eigth row of the checker board.
		 System.out.println();
		 System.out.println( blackSpace+ whiteSpace + blackSpace + whiteSpace + blackSpace+ whiteSpace + blackSpace + whiteSpace);
		 System.out.println("================================================================" + "\n" +
				 			"=========================== CHECKERS ==========================="); 
				 	  
				
	 }

	
	public static String getFieldElement(int r, int c) {
		return field[r][c];
	}
	
	public static void movePiece(int r1,int c1,int r2,int c2) {
		// swap the piece at the r1 c1 position to the r2 c2 position
		String piece = field[r1][c1];
		field[r1][c1] = field[r2][c2];
		field[r2][c2] = piece;
	}

	public static void crownPiece(int row, int column, char reg, char king) {
		// TODO Auto-generated method stub
		field[row][column] = field[row][column].replace(reg, king);
		
	}
	
	public static void clearSpace(int row, int col) {
		field[row][col] = whiteSpace;
	}
	
	// As soon as both players move we will want to perhaps make one or two more methods. A method for updating the board for scanning all positions and
	// making sure
}

/*
 * 
 * Below is a graphical representation of the GameField
 * Each coordinate corresponds to a FieldPosition object.
 * [0,0][0,1][0,2][0,3][0,4][0,5][0,6][0,7]
 * [1,0] ...							.
 * [2,0]								.
 * [3,0]								.
 * [4,0]
 * [5,0]
 * [6,0]
 * [7,0] ...						  [7,7]
 * 
 */

