import java.util.Scanner;


public class Player {
	/* A Player has 
	 * -12 checker pieces to start
	 * -Can move once
	 * -Can lose by not having anymore pieces left
	 * -Can lose by not having anymore moves
	 * -Can lose by resigning
	 */
	private CheckerPiece myPieces[];
	private CheckerPiece myOppPieces[];
	private final String moveLowerLeft = "Enter 1 to move to Lower Left | ",
				  		 moveLowerRight = "Enter 2 to move to Lower Right | ", 
				  		 moveUpperLeft = "Enter 3 to move to Upper Left | ",
				  		 moveUpperRight = "Enter 4 to move to Upper Right | ";
	private final String jumpLowerLeft = "Enter 1 to jump ",
	  		 			 jumpLowerRight = "Enter 2 to jump ", 
	  		 			 jumpUpperLeft = "Enter 3 to jump ",
	  		 			 jumpUpperRight = "Enter 4 to jump ";
	private char playersColor;							
	
	public Player(char color, boolean startedAtBottom)
	{
		myPieces = new CheckerPiece[12];
		
		for(int i = 0;i < 12;i++)
		{
			// Make a new checker piece
			
			CheckerPiece piece = new CheckerPiece(startedAtBottom,i);
			piece.setColor(color);
			// Add it to the collection of pieces.			
			myPieces[i] = piece;
		}
		playersColor = color;
	}
	
	public CheckerPiece[] getPieces() {
		return myPieces;
	}
	
	public char getColor() {
		return playersColor;
	}
	
	public void lookAtOppPieces(CheckerPiece pieces[]) {
		myOppPieces = new CheckerPiece[12];
		for(int i = 1;i <= 12;i++)
		{			
			myOppPieces[i-1] = pieces[i-1];
		}
	}
	
	public CheckerPiece searchForPiece(int row, int column){
		for(CheckerPiece piece:myOppPieces){
			if(!piece.isJumped() && piece.getCoor()[0] == row && piece.getCoor()[1]== column){
				return piece;
			}
		}
		return null;
	}
	
	public boolean noMorePieces() {
		for(int i=0;i < myPieces.length;i++)
		{
			if(!myPieces[i].isJumped())
			
				return false;
		}
		return true;
	}
	
	public boolean hasLost() {
		// 1. if all the player's checker pieces were jumped.
		if(noMorePieces()){return true;}
		// 2. if player does not have any more moves and does not have any more jumps
		else return !canMove() && !canJump();
		// 3. Or the player has resigned!
		
	}
	
	public boolean canJump() {
		for(int i=0;i < myPieces.length;i++)
		{
			CheckerPiece opUpperLeft=searchForPiece(myPieces[i].getUpperLeft()[0],myPieces[i].getUpperLeft()[1]), 
					 opUpperRight=searchForPiece(myPieces[i].getUpperRight()[0],myPieces[i].getUpperRight()[1]),
					 opLowerLeft=searchForPiece(myPieces[i].getLowerLeft()[0],myPieces[i].getLowerLeft()[1]),
					 opLowerRight=searchForPiece(myPieces[i].getLowerRight()[0],myPieces[i].getLowerRight()[1]);
			if(myPieces[i].canJump(opLowerLeft,opLowerRight,opUpperLeft,opUpperRight)) 
				return true;
		}
		return false;
		
	}
	
	public boolean canMove() {
		for(int i=0;i < myPieces.length;i++)
		{
			if(myPieces[i].canMove())
				return true;
		}
		return false;
	}
	public CheckerPiece[] getMovableCheckerPieces() {
		CheckerPiece[] moveAblePieces = new CheckerPiece[12];
	
		// We check if a piece has an existing neighbor position that is a free space.
		for(int i =0;i<12;i++){
			if(myPieces[i].canMove()){
				moveAblePieces[i] = myPieces[i];
			}

		}
		return moveAblePieces;
	}
	

	
	public CheckerPiece[] getJumpingCheckerPieces() {
		CheckerPiece[] jumpingPieces = new CheckerPiece[12];

		
		// We check if a piece has an existing neighbor position
		for(int i =0;i<12;i++){
			CheckerPiece opUpperLeft=searchForPiece(myPieces[i].getUpperLeft()[0],myPieces[i].getUpperLeft()[1]), 
					 opUpperRight=searchForPiece(myPieces[i].getUpperRight()[0],myPieces[i].getUpperRight()[1]),
					 opLowerLeft=searchForPiece(myPieces[i].getLowerLeft()[0],myPieces[i].getLowerLeft()[1]),
					 opLowerRight=searchForPiece(myPieces[i].getLowerRight()[0],myPieces[i].getLowerRight()[1]);
			// if the player is red he/she is the player? Perhaps make a variable for knowing this is the player and not the
			// human. Or we can just not make a computer player. Or we can make Red the default for the player.
			if(myPieces[i].canJump(opLowerLeft,opLowerRight,opUpperLeft,opUpperRight)){
				jumpingPieces[i] = myPieces[i];
			}

		}
		// We find those coordinates
		
		// We check to see who is on those coordinates: a fellow checker piece, a foe checker piece, or a free position through calling the GameField.
		
		// If a checker piece has a free position in the forward direction that is a movable checker piece.
		return jumpingPieces;
	}	
	
	/**
	 * 
	 * @param piece The piece we are jumping with.
	 */
	public void jump(CheckerPiece piece) {
		Scanner read = new Scanner(System.in);
		int j;
		// TODO Check getJumps
		String whichJump = getJumps(piece);
		
		CheckerPiece opUpperLeft=searchForPiece(piece.getUpperLeft()[0],piece.getUpperLeft()[1]), 
				 opUpperRight=searchForPiece(piece.getUpperRight()[0],piece.getUpperRight()[1]),
				 opLowerLeft=searchForPiece(piece.getLowerLeft()[0],piece.getLowerLeft()[1]),
				 opLowerRight=searchForPiece(piece.getLowerRight()[0],piece.getLowerRight()[1]);

		do{
			System.out.println(whichJump+" : ");
			j = read.nextInt();
			switch(j){
			// moving to lower left.
			case 1:
				if(piece.isKing())
					if(opLowerLeft != null && opLowerLeft.hasLowerLeft())
						piece.jumpLowerLeft(opLowerLeft);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(!piece.startedAtBottom()){							
					if(opLowerLeft != null && opLowerLeft.hasLowerLeft())
						piece.jumpLowerLeft(opLowerLeft);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
				// move to lower right
			case 2:
				if(piece.isKing())
					if(opLowerRight != null && opLowerRight.hasLowerRight())
						piece.jumpLowerRight(opLowerRight);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(!piece.startedAtBottom()){							
					if(opLowerRight != null && opLowerRight.hasLowerRight())
						piece.jumpLowerRight(opLowerRight);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
				// move to upper left
			case 3:
				if(piece.isKing())
					if(opUpperLeft != null && opUpperLeft.hasUpperLeft())
						piece.jumpUpperLeft(opUpperLeft);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(piece.startedAtBottom()){							
					if(opUpperLeft != null && opUpperLeft.hasUpperLeft())
						piece.jumpUpperLeft(opUpperLeft);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
				// move to upper right.
			case 4:
				if(piece.isKing())
					if(opUpperRight != null && opUpperRight.hasUpperRight())
						piece.jumpUpperRight(opUpperRight);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(piece.startedAtBottom()){							
					if(opUpperRight != null && opUpperRight.hasUpperRight())
						piece.jumpUpperRight(opUpperRight);
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
			default:
				System.out.println("Invalid Option!");

			}
		}while(j > 4 || j < 1);
		
	}
	
	// 1. At this point we should not be allowed to call the hasLowerLeft, hasLowerRight, hasUpperLeft, hasUpperRight
	// these methods do not account for whether a piece started at the bottom or top or it is a king. so it may return true
	// even though we do not intend it to.
	
	// 2. 
	
	// Consider breaking this up into two methods. 1 that prints out the moves the player can
	// have for a specific piece and 2 the actual performing the move.
	public void move(CheckerPiece piece) {
		Scanner read = new Scanner(System.in);
		int j;
		String whichMove = getMoves(piece);
				
		do{
			System.out.print(whichMove+" : ");
			j = read.nextInt();
			switch(j){
			// moving to lower left.
			case 1:
				if(piece.isKing())
					if(piece.hasLowerLeft())
						piece.moveToLowerLeft();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(!piece.startedAtBottom()){							
					if(piece.hasLowerLeft())
						piece.moveToLowerLeft();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
				// move to lower right
			case 2:
				if(piece.isKing())
					if(piece.hasLowerRight())
						piece.moveToLowerRight();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(!piece.startedAtBottom()){							
					if(piece.hasLowerRight())
						piece.moveToLowerRight();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
				// move to upper left
			case 3:
				if(piece.isKing())
					if(piece.hasUpperLeft())
						piece.moveToUpperLeft();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(piece.startedAtBottom()){							
					if(piece.hasUpperLeft())
						piece.moveToUpperLeft();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
				// move to upper right.
			case 4:
				if(piece.isKing())
					if(piece.hasUpperRight())
						piece.moveToUpperRight();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				else{ if(piece.startedAtBottom()){							
					if(piece.hasUpperRight())
						piece.moveToUpperRight();
					else{
						j = -1;
						System.out.println("Invalid Option!");
					}
				}else{
					j = -1;
					System.out.println("Invalid Option!");
				}
				}
				break;
			default:
				System.out.println("Invalid Option!");

			}
		}while(j > 4 || j < 1);
	}
	
	public String getMoves(CheckerPiece piece){
		String whichMove = "";
		// kings move.
		if(piece.isKing()){
			whichMove+= (piece.hasLowerLeft()? moveLowerLeft:"");
			whichMove+= (piece.hasLowerRight()? moveLowerRight:"");
			whichMove+= (piece.hasUpperLeft()? moveUpperLeft:"");
			whichMove+= (piece.hasUpperRight()? moveUpperRight:"");
			// pieces moving upwards.
		}else if(piece.startedAtBottom()){
			whichMove+= (piece.hasUpperLeft()? moveUpperLeft:"");
			whichMove+= (piece.hasUpperRight()? moveUpperRight:"");
		}else	{	// pieces moving downwards.
			whichMove+= (piece.hasLowerLeft()? moveLowerLeft:"");
			whichMove+= (piece.hasLowerRight()? moveLowerRight:"");
		}
		return whichMove;
		
	}

	public String getJumps(CheckerPiece piece) {
		String whichJumps = "";
		CheckerPiece opUpperLeft=searchForPiece(piece.getUpperLeft()[0],piece.getUpperLeft()[1]), 
					 opUpperRight=searchForPiece(piece.getUpperRight()[0],piece.getUpperRight()[1]),
					 opLowerLeft=searchForPiece(piece.getLowerLeft()[0],piece.getLowerLeft()[1]),
					 opLowerRight=searchForPiece(piece.getLowerRight()[0],piece.getLowerRight()[1]);
		
		// kings move.
		if(piece.isKing()){
			if(opLowerLeft != null)
				whichJumps+= (opLowerLeft.hasLowerLeft()? jumpLowerLeft+opLowerLeft.toString():"");
			
			if(opLowerRight != null)
				whichJumps+= (opLowerRight.hasLowerRight()? jumpLowerRight+opLowerRight.toString():"");
			if(opUpperLeft != null)
				whichJumps+= (opUpperLeft.hasUpperLeft()? jumpUpperLeft+opUpperLeft.toString():"");
			
			if(opUpperRight != null)
				whichJumps+= (opUpperRight.hasUpperRight()? jumpUpperRight+opUpperRight.toString():"");
			

			// pieces moving upwards.
		}else if(piece.startedAtBottom()){
			if(opUpperLeft != null)
				whichJumps+= (opUpperLeft.hasUpperLeft()? jumpUpperLeft+opUpperLeft.toString():"");
			
			if(opUpperRight != null)
				whichJumps+= (opUpperRight.hasUpperRight()? jumpUpperRight+opUpperRight.toString():"");
		}else	{	// pieces moving downwards.
			if(opLowerLeft != null)
				whichJumps+= (opLowerLeft.hasLowerLeft()? jumpLowerLeft+opLowerLeft.toString():"");
			
			if(opLowerRight != null)
				whichJumps+= (opLowerRight.hasLowerRight()? jumpLowerRight+opLowerRight.toString():"");
		}
		return whichJumps;
		
	}
	
	public String toString() {
		if(playersColor == 'r')
			return "Red Player";
		else return "Black Player";
	}
	

}

